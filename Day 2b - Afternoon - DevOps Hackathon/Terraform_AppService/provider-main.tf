###########################
## Azure Provider - Main ##
###########################
export ARM_USE_MSI=false

# Define Terraform provider
terraform {
  required_version = ">= 0.12"
}

# Configure the Azure provider
provider "azurerm" {
  environment = "dev"
  version = "=2.20.0"
  features {}
}
