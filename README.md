

**  Contacts **

Jonathan Perez - jonathan.perez@bcstechnology.com.au

Ian Lim - ian.lim@bcstechnology.com.au

Ashish Agrawal - ashish.agrawal@bcstechnology.com.au

NOTICE - This repository is public.  Please avoid PII issues.  email addresses listed here may get harvested by spambots.

*You can do a git pull on the repository to get updated information if we update the files during the training.*



**Day 1 Agenda**

IaaS, PaaS, SaaS, and Serverless Architecture

• Case studies

• Virtual Machines

• App Services

• Azure Functions

• Azure AD

• SQL Managed Instance



Whiteboard design session (Architecture, cost, and platform optimization)

----------- Break -----------

• DevOps

• DataOps for Managed SQL

• Whiteboard design session (DevOps)


**Day 2 Agenda**


Introduction

Overview of Azure DevOps

• Azure Boards (Overview and Hands-on Exercises)

Azure Pipelines

• Provisioning Infrastructure using Terraform

• Continuous Delivery to Azure App Service


----------- Break -----------


• Azure Test Plan

• Continuous Delivery to Blob Storage Static Hosting (Serverless)

• Continuous Delivery to Azure Functions

• Hackathon Challenge


---




**references:**

What is DevOps
https://docs.microsoft.com/en-us/azure/devops/learn/what-is-devops
Devops checklist
https://docs.microsoft.com/en-us/azure/architecture/checklist/dev-ops
Basic Web application architecture
https://docs.microsoft.com/en-us/azure/architecture/reference-architectures/app-service-web-app/basic-web-app